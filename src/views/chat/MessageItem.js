import React from 'react'
import _get from 'lodash/get'

import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  root: {
    justifyContent: 'flex-end',
  },
  msToIcon: {
    display: 'block',
    backgroundColor: '#efefef',
    width: '10px',
    height: '10px',
    position: 'absolute',
    top: '11px',
    clipPath: 'polygon(100% 0, 0 0, 100% 100%)',
    left: '8px',
  },
  msToIconMe: {
    clipPath: 'polygon(100% 0, 0 0, 0 100%)',
    left: 'unset',
    right: '8px',
  },
  text: {
    maxWidth: 'fit-content',
    backgroundColor: '#efefef',
    padding: '8px !important',
    borderRadius: '3px',
    borderTopRightRadius: '0px',
  },
})

function MessageItem({data, sender, entry, classes}) {
  const itsMe = sender.name === 'Me'

  return (
    <ListItem className={itsMe&&classes.root}>
      <ListItemText className={classes.text} primary={sender.uuid} secondary={_get(data, 'text', entry)}/>
      {!itsMe 
        ? <span className={classes.msToIcon}></span> 
        : <span className={[classes.msToIcon, classes.msToIconMe].join(' ')}></span>
      }
    </ListItem>
  )
}

export default withStyles(styles)(MessageItem)