import React, { Component } from 'react'

import { PubNub } from 'connectors/pubnub'
import ChatView from 'views/chat'
import './App.css'

class App extends Component {
  render() {
    return (
      <PubNub>
        <ChatView/>
      </PubNub>
    )
  }
}

export default App
