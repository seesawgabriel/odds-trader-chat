import React from 'react'

import Button from '@material-ui/core/Button'

import { useChannels } from 'connectors/pubnub'

export default function ChannelButton(props) {
  const { addChat } = useChannels(true)

  return (
     <Button onClick={() => addChat(props.value)}>{props.children}</Button>
  )
}