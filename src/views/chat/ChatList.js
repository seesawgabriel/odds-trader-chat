import React from 'react'
import _map from 'lodash/map'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { Link } from "react-router-dom"

import { useChannels } from 'connectors/pubnub'

function ChatItem({title}) {
  return (
    <ListItem>
      <ListItemText><Link to={`/chat/${title}`}>{title}</Link></ListItemText>
    </ListItem>
  )
}

export default function ListChats() {
  const { chats } = useChannels()

  return (
    <List>
      {_map(chats, channel => <ChatItem title={channel} key={channel}/>)}
    </List>
  )
}