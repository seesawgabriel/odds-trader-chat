import React from 'react'

import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  root: {
    height: '100%',
    position: 'absolute',
    top: '0px'
  }
})

function Loader({classes}) {
  return (
    <Grid container direction='row' justify='center' alignItems='center' className={classes.root}>
      <CircularProgress/>
    </Grid>
  )
}

export default withStyles(styles)(Loader)