import React, {
  useState,
  useReducer,
  createContext,
  useContext,
  useEffect
} from 'react'

import ChatEngineCore from 'chat-engine'
import Loader from 'components/Loader'

import config from '../config.json'

const getUsername = () => {
  const animals = ['zebra', 'goat', 'cow', 'pig', 'tiger', 'wolf', 'pony', 'antelope']
  return 'test-' + animals[Math.floor(Math.random() * animals.length)]
}

const chatEngine = ChatEngineCore.create(config, {globalChannel: 'Odds-Trader'})
chatEngine.connect(getUsername())

const ChannelsContext = createContext([])

// Init Context for Chat Data 
export const PubNub = ({children}) => {
  const [value, setChat] = useState([])
  const [ready, setReady] = useState(false)

  chatEngine.on('$.ready', () => {
    setReady(true)
  })

  function addChat(name) {
    const chat = new chatEngine.Chat(name)
    chat.on('$.connected', (payload) => {
      if (payload.error) {
        console.log("operation failed w/ payload: ", payload)
      } else {
        setChat([...value, payload.chat.channel])
        console.log("operation done!")
      }
    })
  }

  return (
    <ChannelsContext.Provider value={{value, setChat, addChat}}>
      {ready && children}
      {!ready && <Loader/>}
    </ChannelsContext.Provider>
  )
}

export const useChannels = (actions = false) => {
  const {value, addChat } = useContext(ChannelsContext)

  return {
    chats: value,
    addChat,
  }
}

const sendMessage = (text, channel) => {
  chatEngine.chats[channel].emit('message', {
    text
  })
}

function messagesReducer(state, action) {
  const {sender, data, timetoken} = action.message
  switch(action.type) {
    case 'add-old':
      return [{sender, data, timetoken}, ...state]
    case 'new':
      return [...state, {sender, data, timetoken}]
    default:
      return state
  }
}
export const useChannelChat = (channelId) => {
  const [ messages, dispatch ] = useReducer(messagesReducer, [])
  function onMessage(message) {
    dispatch({type:'new', message})
  }

  useEffect(() => {
    const chat = chatEngine.chats[channelId]
    chat.on('message', onMessage)
    chat.search({event: 'message', limit: 50})
      .on('message', message => dispatch({type: 'add-old', message}))
    return () => chat.off('messages', onMessage) 
  }, [channelId])

  return {
    messages,
    sendMessage
  }
}