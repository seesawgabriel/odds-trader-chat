import React from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom"
import ChannelButton from 'components/ChannelButton'

import ChatList from './ChatList'
import ChannelView from './ChannelView'

export function ChatView() {

  return (
    <React.Fragment>
      <ChannelButton value='room-NBA'>NBA</ChannelButton>
      <ChannelButton value='room-NFL'>NFL</ChannelButton>
      <ChannelButton value='room-NCAAF'>NCAAF</ChannelButton>
      <ChatList/>
    </React.Fragment>
  )
}

export default function Navigation() {
  return (
      <Router>
        <React.Fragment>
          <Route exact path='/' component={ChatView}/>
          <Route path='/chat/:channelId' component={ChannelView}/>
        </React.Fragment>
      </Router>
  )
}