import React, { useState } from 'react'
import _map from 'lodash/map'
import _get from 'lodash/get'

import List from '@material-ui/core/List'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import SendIcon from '@material-ui/icons/Send'

import { useChannelChat } from 'connectors/pubnub'
import MessageItem from './MessageItem'


export function ListMessages({messages}) {
  return (
    <List>
      {_map(messages, message => <MessageItem {...message} key={message.timetoken}/>)}
    </List>
  )
}

export default function ChannelView({match, location}) {
  const channelId = _get(match, 'params.channelId') + _get(location, 'hash')
  const { messages, sendMessage } = useChannelChat(channelId)
  const [message, setMessage] = useState('')
  function handleChange(ev) {
    setMessage(ev.target.value)
  }
  function handleSend() {
    setMessage('')
    sendMessage(message, channelId)
  }

  return (
    <div>
      <ListMessages messages={messages}/>
      <TextField
        onChange={handleChange}
        value={message}
        fullWidth
        InputProps={{
          endAdornment: (
            <InputAdornment position='end'>
              <IconButton onClick={handleSend}><SendIcon/></IconButton>
            </InputAdornment>
          )
        }}
      />
    </div>
  )
}